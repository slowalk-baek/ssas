$(document).ready(function() {
  // 모바일 네비게이션
  $('.open-nav').click(function(){
    $('.nav-mobile').slideToggle(300);
  })
  $('.nav-mobile__global').click(function() {
    $('.nav-mobile').slideToggle(300);
  })

  // 알림마당 탭
  $('.tab__content').hide();
  $('.tab__content:first').show();
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab__content').hide();

    var selectTab = $(this).find('a').attr("href");

    $(selectTab).fadeIn();
  });

  // 스티키 네비게이션
  var stickyNavigation = $(".nav-sticky");
      stickyDiv = "sticky";
      pointHeader = $('.section-1').height();

  $(window).scroll(function() {
    if( $(this).scrollTop() > pointHeader ) {
      stickyNavigation.addClass(stickyDiv);
    } else {
      stickyNavigation.removeClass(stickyDiv);
    }
  });

  // 카카오 지도(오시는 길) 설정
  var mapContainer = document.getElementById('pass_map'), // 지도를 표시할 div 
      mapOption = { 
          center: new daum.maps.LatLng(37.5432269, 126.947433), // 지도의 중심좌표
          level: 4 // 지도의 확대 레벨
      };

  var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

  // 마커가 표시될 위치입니다 
  var markerPosition  = new daum.maps.LatLng(37.5432269, 126.947433); 

  // 마커를 생성합니다
  var marker = new daum.maps.Marker({
      position: markerPosition
  });

  // 마커가 지도 위에 표시되도록 설정합니다
  marker.setMap(map);

  // 센터 안내
  $('.center-info__text').hide();
  $('.center-info__text:first').show();
  $('.center-info__title:first').addClass('active');
  $('.center-info__title').click(function(event) {
    $('.center-info__title').removeClass('active');
    $(this).addClass('active');
    $('.center-info__text').hide();
    var selectTab = $(this).find('a').attr("rel");
    $('#' + selectTab).fadeIn();
  });
});